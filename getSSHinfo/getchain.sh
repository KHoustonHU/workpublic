#!/bin/bash

#returns certificate chain of the supplied webserver

scriptname=$0
verbose=no

function usage(){
  cat <<EOF
Usage: $scriptname [-v, -h] servername port outputfile
    -v    print authorities for certificates
    -h    prints this help message
EOF
  exit 0
}

if [[ $1 = "-h" ]] || [[ $1 = "-v" ]]; then
  server=$2
  port=$3
  filename=$4
else
  server=$1
  port=$2
  filename=$3
fi

if [[ ! -z $server ]]; then
  curl -s --head $server | head -n 1 | grep "HTTP/1.[01] [23].." > /dev/null
else
  printf "URL is null \n"
  usage
  exit
fi

if [[ ! $? == 0 ]]; then
  printf "Bad URL \n"
  usage
  exit
elif [[ -z $port ]] || [[ $port =~ '^[0-9]+$' ]] || (( $port < 1 )) || (( $port > 65535 )); then
  printf "Bad port \n"
  usage
  exit
fi

if [ $# -lt 3 ]; then
  usage
  exit
fi

while getopts :v:h FLAG; do
  case $FLAG in
    h)
      usage
      exit ;;
    v)
      verbose=yes ;;
    \?)
      printf "Invalid flag"
      usage
      exit ;;
  esac
done

shift $((OPTIND-1))

##end of getopts code and preliminary checks

printf "Connecting to $server:$port, saving output to $filename \n"

if [ $verbose = yes ]; then
  openssl s_client -connect $server:$port -showcerts < /dev/null  | awk '/s:/,/^-----END CERT/' > $filename
else
  openssl s_client -connect $server:$port -showcerts < /dev/null  | awk '/^-----BEGIN CERT/,/^-----END CERT/' > $filename
fi

cat $filename
exit 0;

##end of main
