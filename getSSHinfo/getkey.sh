#!/bin/bash

#returns certificate chain of the supplied webserver

declare -i port=0
scriptname=$0
server=$1
filename=$3

function usage(){
  cat <<EOF
Usage: $scriptname servername port outputfile
EOF
  exit 0
}

if [[ $2 =~ '^[0-9]+$' ]]; then
  printf "Port is null \n"
  usage
  exit
fi

port=$2

if [[ ! -z $server ]]; then
  curl -s --head $server | head -n 1 | grep "HTTP/1.[01] [23].." > /dev/null
else
  printf "URL is null \n"
  usage
  exit
fi

if [[ ! $? == 0 ]]; then
  printf "Bad URL \n"
  usage
  exit
elif [[ $port =~ '^[0-9]+$' ]] || (( $port < 1 )) || (( $port > 65535 )); then
  printf "Bad port \n"
  usage
  exit
fi

if [ $# -ne 3 ]; then
  usage
  exit
fi

##end of preliminary checks

printf "Connecting to $server:$port, saving output to $filename \n"

openssl s_client -connect $server:$port < /dev/null | openssl x509 -pubkey -noout > $filename

cat $filename
exit 0;

##end of main
